from setuptools import setup

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name="awesome-app",
    version="0.0.1",
    description="An awesome app to learn stuff about ci/cd",
    author="Xomnia",
    long_description=long_description,
    author_email="ioannis.pantazis@xomnia.com",
    install_requires=[
        "flask==1.0.2",
    ],
    extras_require={
        "test": {
            "pytest==3.8.2",
        },
        "lint": {
            "flake8==3.5.0"
        },
    }
)
